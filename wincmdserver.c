/*
Copyright (c) 2012-2015 Hideki EIRAKU <hdk_2@users.sourceforge.net>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    (1) Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer. 

    (2) Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.  
    
    (3)The name of the author may not be used to
    endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <winsock2.h>

static char buf[4096];

static char *
execprocess (char *p)
{
  PROCESS_INFORMATION pi;
  STARTUPINFO si;

  ZeroMemory (&si, sizeof si);
  si.cb = sizeof si;
  if (!CreateProcessA (NULL, p, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL,
		       NULL, &si, &pi))
    return "CreateProcess failed\r\n";
  CloseHandle (pi.hThread);
  CloseHandle (pi.hProcess);
  return "CreateProcess done\r\n";
}

int STDCALL
WinMain (HINSTANCE hInst, HINSTANCE hPrev, LPSTR lpCmd, int nShow)
{
  WSADATA wsaData;
  SOCKET sock0;
  struct sockaddr_in addr;
  struct sockaddr_in client;
  int len;
  SOCKET sock;
  int port;
  int i;
  char *p;

  WSAStartup (MAKEWORD (2,0), &wsaData);
  while (*lpCmd == ' ' || *lpCmd == '\t')
    lpCmd++;
  port = 0;
  while (*lpCmd)
    {
      if (*lpCmd >= '0' && *lpCmd <= '9')
	port = (port * 10) + (*lpCmd - '0');
      else if (*lpCmd == ' ' || *lpCmd == '\t')
	break;
      else
	goto argerr;
      lpCmd++;
    }
  while (*lpCmd == ' ' || *lpCmd == '\t')
    lpCmd++;
  if (!*lpCmd)
    {
    argerr:
      MessageBox (NULL, "usage: program portnum password", "wincmdserver",
		  MB_OK | MB_ICONINFORMATION);
      return 0;
    }

  sock0 = socket (AF_INET, SOCK_STREAM, 0);
  addr.sin_family = AF_INET;
  addr.sin_port = htons (port);
  addr.sin_addr.S_un.S_addr = INADDR_ANY;
  bind (sock0, (struct sockaddr *)&addr, sizeof addr);
  listen (sock0, 5);
  len = sizeof client;
  for (;;)
    {
      sock = accept (sock0, (struct sockaddr *)&client, &len);
      if (sock < 0)
	{
	  MessageBox (NULL, "accept error", "wincmdserver",
		      MB_OK | MB_ICONINFORMATION);
	  break;
	}
      for (i = 0; i < sizeof buf - 1; i++)
	{
	  if (recv (sock, &buf[i], 1, 0) != 1)
	    break;
	  if (buf[i] == '\r' || buf[i] == '\n')
	    break;
	}
      buf[i] = '\0';
      if (strncmp (buf, lpCmd, strlen (lpCmd)))
	{
	  p = "Password mismatch\r\n";
	  goto done;
	}
      p = buf + strlen (lpCmd);
      while (*p == ' ' || *p == '\t')
	p++;
      if (!strncmp (p, "cd ", 3))
	{
	  p += 3;
	  if (!SetCurrentDirectory (p))
	    {
	      p = "SetCurrentDirectory failed\r\n";
	      goto done;
	    }
	  p = "SetCurrentDirectory done\r\n";
	  goto done;
	}
      p = execprocess (p);
    done:
      send (sock, p, strlen (p), 0);
      closesocket (sock);
    }

  WSACleanup ();

  return 0;
}
